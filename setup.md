# Installation d'Ansible et Configuration de `ansadmin`

## Étape 1: Installation d'Ansible

1. **Mettez à jour la liste des paquets :**

    ```bash
    sudo apt update
    ```

2. **Installez Ansible :**

    ```bash
    sudo apt install -y ansible
    ```

3. **Vérifiez l'installation d'Ansible :**

    ```bash
    ansible --version
    ```

    Vous devriez voir une sortie confirmant la version d'Ansible installée.

## Étape 2: Corriger la configuration sudo pour `ansadmin`

1. **Accédez à la machine avec des droits superutilisateur (root)** ou utilisez un autre utilisateur avec des droits `sudo`.
2. **Ouvrez le fichier `/etc/sudoers` avec `visudo`** pour vérifier la syntaxe avant d'enregistrer les modifications :

    ```bash
    sudo visudo
    ```

3. **Recherchez et corrigez la ligne incorrecte**. Remplacez :

    ```plaintext
    ansadmin ALL=(ALL) NOPASSWORD:ALL
    ```

    par :

    ```plaintext
    ansadmin ALL=(ALL) NOPASSWD:ALL
    ```

4. **Enregistrez et quittez `visudo`** (`Ctrl + X`, puis `Y` pour confirmer l'enregistrement).

## Étape 3: Créer et configurer le répertoire `.ssh` pour `ansadmin`

1. **Reconnectez-vous en tant que `ansadmin` ou utilisez un compte avec les droits nécessaires**.
2. **Créez le répertoire `.ssh` et configurez les permissions :**

    ```bash
    sudo mkdir -p /home/ansadmin/.ssh
    sudo chown ansadmin:ansadmin /home/ansadmin/.ssh
    sudo chmod 700 /home/ansadmin/.ssh
    ```

3. **Créez ou éditez le fichier `authorized_keys` et configurez les permissions :**

    ```bash
    sudo touch /home/ansadmin/.ssh/authorized_keys
    sudo chown ansadmin:ansadmin /home/ansadmin/.ssh/authorized_keys
    sudo chmod 600 /home/ansadmin/.ssh/authorized_keys
    ```

4. **Ajoutez la clé publique**. Si vous avez la clé publique sur votre machine locale, copiez-la :

    ```bash
    echo "votre_clé_publique_ici" | sudo tee -a /home/ansadmin/.ssh/authorized_keys
    ```

## Étape 4: Vérifiez les permissions et réessayez la connexion SSH

1. **Assurez-vous que les permissions sont correctement définies :**

    ```bash
    ls -ld /home/ansadmin/.ssh
    ls -l /home/ansadmin/.ssh/authorized_keys
    ```

    Les permissions devraient ressembler à ceci :

    ```plaintext
    drwx------ 2 ansadmin ansadmin 4096 Jan 1 00:00 /home/ansadmin/.ssh
    -rw------- 1 ansadmin ansadmin  397 Jan 1 00:00 /home/ansadmin/.ssh/authorized_keys
    ```

2. **Essayez de vous connecter à nouveau :**

    ```bash
    ssh ansadmin@172.31.30.139
    ```

## Conclusion

En suivant ces étapes, vous aurez installé Ansible, corrigé la configuration sudo pour `ansadmin`, et configuré correctement les clés SSH. Vous êtes maintenant prêt à utiliser Ansible pour automatiser vos tâches de gestion de configuration.

---

Je suis toujours là pour vous aider ! Si vous avez des questions supplémentaires ou des problèmes, n'hésitez pas à demander.
